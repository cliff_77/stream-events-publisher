/*global module:true, require:true */
/*jslint white:true, unparam:true, nomen:true */

'use strict';

var rabbitHub = require('rabbitmq-nodejs-client');
var url = require('url');
var events = require('events');
var path = require('path');

var envHost = process.env.RABBITMQ_HOST || 'slc-mobiledev.dev-cms.com';

function camelize( s )
{
    var result = s.replace(/(\-|_|\s)+([\w\W])?/g, function( mathc, sep, c )
    {
        /* istanbul ignore next */
        return (c ? c.toUpperCase() : '');
    });
    return result;
}

var StreamEventsPublisher = function()
{
    this.logger = this.pubHub = this.hub = null;
};

StreamEventsPublisher.varName = function()
{
    return camelize(require(path.join(__dirname, '..', 'package')).name);
};

StreamEventsPublisher.prototype = new events.EventEmitter();

StreamEventsPublisher.prototype.init = function( server, logger )
{
    this.logger = logger;
    server.publishEvent = this.publishEvent;

    if (!envHost)
    {
        this.emit('INIT_ERR', server);
        /* istanbul ignore if */
        if (this.logger)
        {
            this.logger.debug('StreamEventsPublisher failed to initialize.');
        }
        return;
    }

    //this.client = rabbitHub.createClient(parsedUrl.port, parsedUrl.hostname);
    this.pubHub = rabbitHub.create( { host: envHost, task: 'pub', channel: 'events' } );
    this.pubHub.once('connection', function(hub)
    {
        this.hub = hub;
        this.emit('INIT_OK', server);
        /* istanbul ignore if */
        if (this.logger)
        {
            this.logger.debug('StreamEventsPublisher RabbitMQ client connection established');
        }

    }.bind(this));
    this.pubHub.on('error', function( err )
    {
        this.emit('INIT_ERR', err);
        /* istanbul ignore if */
        if (this.logger)
        {
            this.logger.debug('StreamEventsPublisher RabbitMQ client error: ', err);
        }
    }.bind(this));
    
    this.pubHub.connect();
};

StreamEventsPublisher.prototype.publishEvent = function( data, callback )
{
    var plugin = this[StreamEventsPublisher.varName()];
    plugin.hub.send(data);
};


module.exports = StreamEventsPublisher;
