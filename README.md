

# stream-events-publisher

RabbitMQ based distributed event message queue plugin for the Stream Events server

## Install
Run the following command in the root of your stream-events-api cloned directory
	
	npm install stream-events-publisher --save

## Usage
	
This is a plugin for the stream-events-api.  All that is needed to use this is to
make sure you have installed the package stream-events-publisher and added the package and version
to the package.json found in the root of the application.

Once installed this module will instantiate an instance of StreamEventsPublisher which makes a 
connection to a remote RabbitMQ client.  It will then attach the instantiated StreamEventsPublisher object to
the server as well as add the method publishEvent to the server.  

Example: (code should be in the routes file index.js of the proper service folder)

	module.exports = function( server )
	{
		server.get('/hello', function( req, res, next )
    	{
    		server.publishEvent('Hello Event');
        	res.send({ msg: 'thanks'});
    	}); 
	};




##Environment Variables

### RabbitMQ host to use
RABBITMQ_HOST

## Exports

### StreamEventPublisher 
An object definition that will be instantiated and attached to the server at runtime.






