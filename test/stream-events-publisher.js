/*global describe:true, it:true, module:true, require:true */
/*jslint white:true, unparam:true, nomen:true */
/*jshint expr: true*/

'use strict';

var should = require('should');
var rewire = require('rewire');
var path = require('path');
var rabbitMQMock = require(path.join(__dirname, 'mocks', 'rabbitmq'));
var rabbitMQErrorMock = require(path.join(__dirname, 'mocks', 'rabbitmq-error'));
var rabbitHub = require('rabbitmq-nodejs-client');

function createStreamEventsPublisher( envHost, rabbitMQ )
{
    console.log("creating stream events publisher");
    var StreamEventsPublisher = rewire('../lib/index.js');
    StreamEventsPublisher.__set__('envHost', envHost);
    StreamEventsPublisher.__set__('rabbitHub', rabbitMQ);
    return new StreamEventsPublisher();
}

describe('stream-events-publisher', function()
{
    var rabbitMQFakeHost = 'fakehost';
    describe('#init', function()
    {
        it('Should initialize', function( initDone )
        {
          console.log("Should initialize first");
            var pub = createStreamEventsPublisher(rabbitMQFakeHost, rabbitMQMock);
            pub.once('INIT_OK', function( server )
            {
                initDone();
            });

            pub.once('INIT_ERR', function( server )
            {
                throw new Error('Failed To Initialize');
            });
            pub.init({});
        });

        it('Should fail to initialize with no envHost', function( initFailDone )
        {
            var pub = createStreamEventsPublisher('', rabbitMQMock);
            pub.once('INIT_OK', function( server )
            {
                throw new Error('Should not have initialized');
            });

            pub.once('INIT_ERR', function( server )
            {
                initFailDone();
            });
            pub.init({});
        });

        it('Should fail with rabbitMQ connect error', function( connectFailDone )
        {
            var pub = createStreamEventsPublisher(rabbitMQFakeHost, rabbitMQErrorMock);
            pub.once('INIT_OK', function( server )
            {
                throw new Error('Should not have initialized');
            });

            pub.once('INIT_ERR', function( server )
            {
                connectFailDone();
            });
            pub.init({});
        });

    });

    describe('#publishEvent', function()
    {
        it('Should successfully publish event', function(publishDone)
        {
            var StreamEventsPublisher = rewire('../lib/index.js');
            rabbitMQMock.getReturnError = null;
            rabbitMQMock.getReturnValue = 'test';
            StreamEventsPublisher.__set__('envHost', rabbitMQFakeHost);
            StreamEventsPublisher.__set__('rabbitHub', rabbitMQMock);
            var pub = new StreamEventsPublisher();
            var server = require(path.join(__dirname, 'mocks', 'server'));
            pub.init(server);
            server[StreamEventsPublisher.varName()] = pub;
            
            pub.once('INIT_OK', function( server ) 
            {
              server.publishEvent('/test');
              publishDone();
            });
        });
    });
});
